# sbt

Interactive build tool for Scala, Java, and more. [sbt](https://www.archlinux.org/packages/?q=sbt)

# Selecting java version with pacman
```
:: There are 4 providers available for java-runtime:
:: Repository extra
   1) jre-openjdk  2) jre11-openjdk  3) jre7-openjdk  4) jre8-openjdk
Enter a number (default=1): 
```
In non-interactive mode, the default answer may lead to the installation of two version of the jre.